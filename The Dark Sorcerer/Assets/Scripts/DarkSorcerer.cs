﻿using System.Collections;
using UnityEngine;

public class DarkSorcerer : MonoBehaviour
{
    [SerializeField] private float _speed = 1;
    [SerializeField] private float _shootCooldown = 3;
    [SerializeField] private bool _gizmos = true;
    [SerializeField] private Transform _projectile;
    [SerializeField] private Transform _shootPosition;
    [SerializeField] private ParticleSystem _deathEffect;
    [SerializeField] private float _hitPoints = 2;
    [SerializeField] private Transform _deathSprite;
    private Vector3 _direction;
    private Animator _anim;
    private GameManager _manager;
    private Coroutine _coroutineShooting;
    private BackgroundMusic _music;

    private void Start()
    {
        _music = FindObjectOfType<BackgroundMusic>();
        _anim = GetComponent<Animator>();
        _coroutineShooting = StartCoroutine(IEShooting());
        _manager = GameManager.Instance;
    }

    private void Update()
    {
    }

    private void Flip()
    {
        transform.Rotate(0, 180, 0);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            Player p = collision.gameObject.GetComponent<Player>();
            p.Hit();
            Destroy(gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        if (!_gizmos)
            return;
    }

    private IEnumerator IEShooting()
    {
        bool shooting = true;
        while (shooting)
        {
            yield return new WaitForSeconds(_shootCooldown);
            Shoot();
        }
    }

    private void Shoot()
    {
        _anim.SetTrigger("Shoot");
        Instantiate(_projectile, _shootPosition.position, Quaternion.Euler(0, 0, transform.eulerAngles.y));
    }

    public void Hit()
    {
        _hitPoints--;
        Instantiate(_deathEffect, transform.position, Quaternion.identity);

        // Boss is dead
        if (_hitPoints <= 0)
        {
            _manager.BossDefeated();
            Instantiate(_deathSprite, transform.position, Quaternion.identity);
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<Rigidbody2D>().gravityScale = 0;
            StopCoroutine(_coroutineShooting);
            _anim.SetBool("IsDead", true);
            _music.PlayBossDefeatedMusic();
        }
    }

    public void MoveUp(float duration) => StartCoroutine(IEMoveUp(duration));

    private IEnumerator IEMoveUp(float duration)
    {
        float lerpTime = duration; // duration of lerping
        float currentLerpTime = 0f; ;
        Vector3 startPos = transform.position;
        Vector3 endPos = transform.position + 2 * Vector3.up;

        while (true)
        {
            // increment timer once per frame
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime)
            {
                currentLerpTime = lerpTime;
            }

            // lerp!
            float t = currentLerpTime / lerpTime;
            t = t = t * t * (3f - 2f * t); // Easing: Smoothstep
            transform.position = Vector3.Lerp(startPos, endPos, t);

            yield return null;
        }
    }

    public void MoveToPosition(float duration, Vector2 pos) => StartCoroutine(IEMoveToPosition(duration, pos));
    private IEnumerator IEMoveToPosition(float duration, Vector2 pos)
    {
        yield return new WaitForEndOfFrame();

        float lerpTime = duration; // duration of lerping
        float currentLerpTime = 0f; ;
        Vector3 startPos = transform.position;
        Vector3 endPos = pos;
        bool run = true;

        while (run)
        {
            // increment timer once per frame
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime)
            {
                currentLerpTime = lerpTime;
            }

            // lerp!
            float t = currentLerpTime / lerpTime;
            t = t = t * t * (3f - 2f * t); // Easing: Smoothstep
            transform.position = Vector3.Lerp(startPos, endPos, t);

            if (t >= 1)
            {
                run = false;
                Destroy(gameObject);
            }

            yield return null;
        }
    }
}