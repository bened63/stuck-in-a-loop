﻿using System.Collections;
using UnityEngine;

public class Spell : MonoBehaviour
{
    public enum Caster
    {
        Player, Enemy, Boss
    }

    [SerializeField] private float _speed = 10f;
    [SerializeField] private float _lifetime = 5f;
    [SerializeField] private Caster _caster = Caster.Player;
    [SerializeField] private AudioClip _explosion;
    [SerializeField] private Transform _explosionPrefab;
    private Camera _mainCam;
    private Vector3 _direction;

    private void Start()
    {
        _mainCam = Camera.main;
        _direction = transform.right;
        StartCoroutine(IEDestroy(_lifetime, false));
    }

    private void FixedUpdate()
    {
        transform.position += _direction * Time.fixedDeltaTime * _speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<Enemy>().Die();
        }
        if (collision.transform.CompareTag("Boss"))
        {
            collision.gameObject.GetComponent<DarkSorcerer>().Hit();
        }
        if (collision.transform.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Player>().Hit();
        }


        StopAllCoroutines();
        StartCoroutine(IEDestroy(0, true));
    }

    private IEnumerator IEDestroy(float wait, bool explode)
    {
        yield return new WaitForSeconds(wait);
        if (explode && _explosionPrefab != null)
        {
            AudioSource.PlayClipAtPoint(_explosion, _mainCam.transform.position + new Vector3(0, 0, 1), 0.7f);
            Transform t = Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
            Destroy(t.gameObject, 0.2f);
        }
        Destroy(gameObject);
    }
}