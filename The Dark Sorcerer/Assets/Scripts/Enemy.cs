﻿using System.Collections;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float _speed = 1;
    [SerializeField] private float _moveRange = 3;
    [SerializeField] private float _shootCooldown = 3;
    [SerializeField] private bool _gizmos = true;
    [SerializeField] private Transform _projectile;
    [SerializeField] private Transform _shootPosition;
    [SerializeField] private ParticleSystem _deathEffect;
    [SerializeField] private float _minX, _maxX;
    private Vector3 _direction;

    private void Start()
    {
        _direction = transform.right;
        _minX = transform.position.x - _moveRange;
        _maxX = transform.position.x + _moveRange;

        StartCoroutine(IEShooting());
    }

    // Update is called once per frame
    private void Update()
    {
        if (!Application.isPlaying)
            return;

        if (transform.position.x <= _minX || transform.position.x >= _maxX) Flip();
        transform.Translate(_direction * Time.deltaTime * _speed);
    }

    private void Flip()
    {
        //_direction = -_direction;
        transform.Rotate(0, 180, 0);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            Player p = collision.gameObject.GetComponent<Player>();
            p.Hit();
            Die();
        }
    }

    private void OnDrawGizmos()
    {
        if (!_gizmos)
            return;

        Gizmos.color = Color.red;
        Gizmos.DrawCube(transform.position, new Vector3(2 * _moveRange, 0.05f, 0));
    }

    private IEnumerator IEShooting()
    {
        bool shooting = true;
        while (shooting)
        {
            yield return new WaitForSeconds(_shootCooldown);
            Shoot();
        }
    }

    private void Shoot()
    {
        Instantiate(_projectile, _shootPosition.position, Quaternion.Euler(0, 0, transform.eulerAngles.y));
    }

    public void Die()
    {
        ParticleSystem ps = Instantiate(_deathEffect, transform.position, Quaternion.identity);
        Destroy(ps.gameObject, 0.8f);
        Destroy(gameObject);
    }
}