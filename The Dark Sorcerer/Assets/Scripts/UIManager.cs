﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject _start;
    [SerializeField] private GameObject _gameOver;
    [SerializeField] private GameObject _win;

    private void Start()
    {
        StartCoroutine(IEHideShowMessage(3));
    }

    public void ShowGameOverMessage()
    {
        _gameOver.SetActive(true);
    }

    public void ShowWinMessage()
    {
        _win.SetActive(true);
    }

    IEnumerator IEHideShowMessage(float delay)
    {
        yield return new WaitForSeconds(delay);
        _start.SetActive(false);
        yield return null;
    }
}
