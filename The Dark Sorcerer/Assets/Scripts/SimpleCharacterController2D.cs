﻿using UnityEngine;

public class SimpleCharacterController2D : MonoBehaviour
{
    [SerializeField] private float _jumpForce = 10;
    [SerializeField] private Transform _groundCheck;
    [SerializeField] private float _groundCheckRadius = 0.1f;
    [SerializeField] private LayerMask _whatIsGround;
    [SerializeField] private bool _showGizmos = true;
    private Rigidbody2D _rb2D;
    private bool _grounded;
    private bool _facingRight = true;

    public bool grounded { get => _grounded; }

    private void Start()
    {
        _rb2D = GetComponent<Rigidbody2D>();
        _grounded = false;
    }

    private void Update()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(_groundCheck.position, _groundCheckRadius, _whatIsGround);
        if (colliders.Length > 0) _grounded = true;
        else _grounded = false;
    }

    private void OnDrawGizmos()
    {
        if (!_showGizmos)
            return;

        Gizmos.color = _grounded ? Color.green : Color.red;
        Gizmos.DrawWireSphere(_groundCheck.position, _groundCheckRadius);
    }

    public void Move(float move, bool crouch = false, bool jump = false)
    {
        Vector3 targetVelocity = new Vector2(move * 10f, _rb2D.velocity.y);
        _rb2D.velocity = targetVelocity;

        if (_grounded && jump)
        {
            _rb2D.AddForce(new Vector2(0f, _jumpForce));
            _grounded = false;
        }

        if (move > 0 && !_facingRight) Flip();
        else if (move < 0 && _facingRight) Flip();
    }

    public void Flip(int angle = -1)
    {
        _facingRight = !_facingRight;
        if (angle == -1) transform.Rotate(0, 180, 0);
        else transform.eulerAngles = new Vector3 (0, angle, 0);
    }
}