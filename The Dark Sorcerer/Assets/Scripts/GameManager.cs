﻿using System;
using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton Handling

    protected static GameManager instance;

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                Debug.LogWarning("ERROR: GameManager not initialized");
            }
            return instance;
        }
    }

    private void Awake()
    {
        if (instance == null) instance = this;
        DontDestroyOnLoad(gameObject);
    }

    #endregion Singleton Handling

    [SerializeField] private Player _player;
    [SerializeField] private SecondPlayer _secondPlayer;
    [SerializeField] private DarkSorcerer _darkSorcerer;

    public bool bossDefeated { get; private set; } = false;

    public Action onBossDefeated;

    private void Start()
    {
    }

    public void BossDefeated()
    {
        Spell[] projectiles = FindObjectsOfType<Spell>();
        foreach (var item in projectiles)
        {
            Destroy(item.gameObject);
        }

        Enemy[] enemies = FindObjectsOfType<Enemy>();
        foreach (var item in enemies)
        {
            Destroy(item.gameObject);
        }

        bossDefeated = true;
        onBossDefeated?.Invoke();
        StartCoroutine(IEEndSequence());
    }

    protected IEnumerator IEEndSequence()
    {
        float elapsedTime = 0f;
        float updateRate = 1f / 30f;

        // calc passed time
        elapsedTime += Time.deltaTime;

        // do stuff
        if (true)
        {
            elapsedTime = 0f;

            if (_darkSorcerer == null) _darkSorcerer = FindObjectOfType<DarkSorcerer>();
            if (_player == null) _player = FindObjectOfType<Player>();

            yield return new WaitForSeconds(1f);

            // Move boss slightly up
            _darkSorcerer.MoveUp(4);
            yield return new WaitForSeconds(5f);

            // Move fast to Players' position
            _darkSorcerer.MoveToPosition(0.2f, _player.transform.position);
            yield return new WaitForSeconds(0.7f);

            // Turn Player red
            _player.TurnRed(0.5f);

            yield return new WaitForSeconds(1.7f);

            // Let another Player come from the left
            SecondPlayer p2 = Instantiate(_secondPlayer, _player.transform.position + new Vector3(-10, 10, 0), Quaternion.identity);
            p2.Appear(0);
            //p2.AddForce(new Vector2(200, 0));
            //yield return new WaitUntil(() => (_player.transform.position.x - p2.transform.position.x) < 10);
            //p2.AddForce(new Vector2(-200, 0));

            // Let another Player kill you
        }

        //next frame
        yield return null;
    }

    //protected IEnumerator Lerp()
    //{
    //    float lerpTime = 1f; // duration of lerping
    //    float currentLerpTime = 0f; ;
    //    Vector3 startPos = Vector3.zero; // TODO: Edit
    //    Vector3 endPos = Vector3.up; // TODO: Edit

    //    while (true)
    //    {
    //        // increment timer once per frame
    //        currentLerpTime += Time.deltaTime;
    //        if (currentLerpTime > lerpTime)
    //        {
    //            currentLerpTime = lerpTime;
    //        }

    //        // lerp!
    //        float t = currentLerpTime / lerpTime;
    //        t = t = t * t * (3f - 2f * t); // Easing: Smoothstep
    //        transform.position = Vector3.Lerp(startPos, endPos, t);

    //        yield return null;
    //    }

    //}
}