﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] private float _runSpeed = 30;
    [SerializeField] private Transform _hand;
    [SerializeField] private Transform _spell;
    [SerializeField] private ParticleSystem _deathEffect;
    [SerializeField] private Transform _deathSprite;
    [SerializeField] private AudioClip _shootClip;
    [SerializeField] private AudioClip _dieClip;
    [SerializeField] private AudioClip _jumpClip;
    [SerializeField] private float _fireCooldown;
    private float _lastFired;
    private AudioSource _audioSource;
    private bool _hasTorch = true;
    private SimpleCharacterController2D _controller;
    private float _horizontal;
    [SerializeField] private bool _jump = false;
    private Animator _anim;
    private UIManager _ui;
    private bool _isDead = false;
    private bool _bossDefeated = false;
    private GameManager _manager;
    private BackgroundMusic _music;

    // Start is called before the first frame update
    private void Start()
    {
        _music = FindObjectOfType<BackgroundMusic>();
        _audioSource = GetComponent<AudioSource>();
        _controller = GetComponent<SimpleCharacterController2D>();
        _anim = GetComponent<Animator>();
        _ui = FindObjectOfType<UIManager>();
        if (_ui == null) Debug.Log("<color=lime>No UIManager found.</color>");
        _manager = GameManager.Instance;
        _manager.onBossDefeated += () => _bossDefeated = true;
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }

        if (_isDead || _bossDefeated)
        {
            _horizontal = 0;
            return;
        }

        _horizontal = Input.GetAxisRaw("Horizontal") * _runSpeed;
        if (_horizontal != 0) _anim.SetBool("IsRunning", true);
        else _anim.SetBool("IsRunning", false);

        if (_controller.grounded && Input.GetKeyDown(KeyCode.W))
        {
            _audioSource.clip = _jumpClip;
            _audioSource.Play();
            _anim.SetTrigger("IsJumping");
            _jump = true;
        }

        Fire();
    }

    private void FixedUpdate()
    {
        _controller.Move(_horizontal * Time.fixedDeltaTime, false, _jump);
        _jump = false;
    }

    private void Fire()
    {
        _lastFired += Time.deltaTime;
        if (_lastFired >= _fireCooldown && Input.GetButtonDown("Jump"))
        {
            _lastFired = 0;
            _audioSource.clip = _shootClip;
            _audioSource.Play();
            _anim.SetTrigger("Shoot");
            Instantiate(_spell, _hand.position, Quaternion.Euler(0, 0, _hand.eulerAngles.y));
        }
    }

    public void Hit()
    {
        if (_isDead)
            return;

        Instantiate(_deathEffect, transform.position, Quaternion.identity);
        Instantiate(_deathSprite, transform.position, Quaternion.identity);
        _isDead = true;

        _audioSource.clip = _dieClip;
        _audioSource.Play();

        // Remove Player from screen
        GetComponent<SpriteRenderer>().enabled = false;

        // TODO: Boss shoould stop firing
        // Disable Collider so Player cannot be hit by another projectile
        //GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<Rigidbody2D>().gravityScale = 0; // Stop Player from falling throug level

        _music.StopMusic();

        if (_manager.bossDefeated && FindObjectOfType<SecondPlayer>())
        {
            _ui.ShowWinMessage();
        }
        else
        {
            _ui.ShowGameOverMessage();
        }
    }

    public void TurnRed(float duration) => StartCoroutine(IETurnRed(duration));

    private IEnumerator IETurnRed(float duration)
    {
        yield return new WaitForEndOfFrame();

        float lerpTime = duration; // duration of lerping
        float currentLerpTime = 0f; ;
        Color start = Color.white;
        Color end = new Color32(108, 28, 28, 255);
        bool run = true;

        while (run)
        {
            // increment timer once per frame
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime)
            {
                currentLerpTime = lerpTime;
            }

            // lerp!
            float t = currentLerpTime / lerpTime;
            t = t = t * t * (3f - 2f * t); // Easing: Smoothstep
            GetComponent<SpriteRenderer>().color = Color.Lerp(start, end, t);

            if (t >= 1)
            {
                run = false;
                yield return new WaitForSeconds(1);
                _controller.Flip(180);
            }

            yield return null;
        }
    }
}