﻿using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{
    //AudioSource[] _audioSources;
    [SerializeField] private AudioClip _normal;
    [SerializeField] private AudioClip _boss;
    [SerializeField] private AudioClip _bossDefeated;
    private AudioSource _audioSource;
    
    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void StopMusic()
    {
        _audioSource.Stop();
    }

    public void PlayNormalMusic()
    {
        _audioSource.clip = _normal;
        _audioSource.Play();
    }

    public void PlayBossMusic()
    {
        _audioSource.clip = _boss;
        _audioSource.Play();
    }

    public void PlayBossDefeatedMusic()
    {
        _audioSource.clip = _bossDefeated;
        _audioSource.Play();
    }
}