﻿using UnityEngine;

[ExecuteInEditMode]
public class CameraController : MonoBehaviour
{
    [SerializeField] private Player _player;
    [SerializeField] private Vector2 _offset = new Vector2(3,3);
    [SerializeField] private Vector2 _clampX = new Vector2(-500,500);

    private void Start()
    {
    }

    private void Update()
    {
        Vector3 pos = _player.transform.position;
        pos.x = Mathf.Clamp(pos.x + _offset.x, _clampX.x, _clampX.y);
        pos.y = _offset.y;
        pos.z = transform.position.z;
        transform.position = pos;
    }
}