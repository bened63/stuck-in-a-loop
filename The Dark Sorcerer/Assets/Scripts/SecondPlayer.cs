﻿using System.Collections;
using UnityEngine;

public class SecondPlayer : MonoBehaviour
{
    [SerializeField] private float _runSpeed = 30;
    [SerializeField] private Transform _hand;
    [SerializeField] private Transform _spell;
    [SerializeField] private ParticleSystem _deathEffect;
    [SerializeField] private Transform _deathSprite;
    [SerializeField] private Transform _speakText;
    private bool _hasTorch = true;
    private SimpleCharacterController2D _controller;
    private float _horizontal;
    private bool _jump = false;
    private Animator _anim;
    private UIManager _ui;
    private bool _isDead = false;
    private bool _bossDefeated = false;
    private GameManager _manager;

    // Start is called before the first frame update
    private void Start()
    {
        _controller = GetComponent<SimpleCharacterController2D>();
        _anim = GetComponent<Animator>();
        _ui = FindObjectOfType<UIManager>();
        if (_ui == null) Debug.Log("<color=lime>No UIManager found.</color>");
        _manager = GameManager.Instance;
        _manager.onBossDefeated += () => _bossDefeated = true;
    }

    public void Fire()
    {
        _anim.SetTrigger("Shoot");
        Instantiate(_spell, _hand.position, Quaternion.Euler(0, 0, _hand.eulerAngles.y));
    }

    public void AddForce(Vector2 force)
    {
        GetComponent<Rigidbody2D>().AddForce(force);
    }

    public void Appear(float duration) => StartCoroutine(IEAppear(duration));

    private IEnumerator IEAppear(float duration)
    {
        GetComponent<Rigidbody2D>().AddForce(new Vector2(200, 0));
        yield return new WaitForSeconds(1);
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        yield return new WaitForSeconds(1.25f);
        _speakText.gameObject.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        _speakText.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        Fire();
    }
}