﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float _speed = 3;
    private Rigidbody _rb;
    private Vector3 _direction;
    private Vector3 _lastVelocity;
    private GameManager _gameManager;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _direction = transform.forward;
        _rb.velocity = _direction * _speed;

        _gameManager = FindObjectOfType<GameManager>();
        if (_gameManager == null) Debug.Log("<color=lime>No GameManager found!</color>");

        _gameManager.onStartChangingRing += () => { if (_rb != null) _rb.velocity = transform.forward * 0; };
        _gameManager.onEndChangingRing += () =>
        {
            if (_rb != null) _rb.velocity = transform.forward * _speed;
        };
    }

    private void Update()
    {
    }

    private void FixedUpdate()
    {
        _lastVelocity = _rb.velocity;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Player p = collision.gameObject.GetComponent<Player>();
            p.Hit();
            Destroy(gameObject);
        }

        _direction = Vector3.Reflect(_lastVelocity.normalized, collision.contacts[0].normal);
        _rb.velocity = _direction * _speed;
    }
}