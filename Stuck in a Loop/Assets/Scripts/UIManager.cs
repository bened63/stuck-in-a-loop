﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Text _level;
    [SerializeField] private Text _ballCount;

    void Start()
    {
        ShowLevel(0);
        ShowBallCount(0);
    }

    public void ShowBallCount(int count)
    {
        _ballCount.text = string.Format("Balls: {0}", count);
    }

    public void ShowLevel(int lvl)
    {
        _level.text = string.Format("Level: {0}", lvl);
    }
}
