﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] private float _speedMax = 200;
    private float _speed;
    [SerializeField] private Transform _projectile;
    [SerializeField] private Transform _projectileSpawnPoint;
    private int _ballCount;
    private Rigidbody _rb;
    private Vector3 _velocity;
    private Camera _mainCam;
    private Transform _projectileContainer;
    private GameManager _gameManager;
    private UIManager _ui;

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _mainCam = Camera.main;
        _projectileContainer = new GameObject("ProjectileContainer").transform;
        _speed = _speedMax;

        _gameManager = FindObjectOfType<GameManager>();
        if (_gameManager == null) Debug.Log("<color=lime>No GameManager found!</color>");

        _ui = FindObjectOfType<UIManager>();
        if (_ui == null) Debug.Log("<color=lime>No UIManager found!</color>");

        _gameManager.onStartChangingRing += () => { _speed = 0; };
        _gameManager.onEndChangingRing += () => { _speed = _speedMax; };
    }

    private void Update()
    {
        // Movement
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        _velocity = new Vector3(horizontal, 0, vertical);
        _velocity.Normalize(); // Fixes diagonal increased speed

        // Look at mouse
        LookAtMouse();

        // Fire spheres
        Fire();
    }

    // Physics update methode
    private void FixedUpdate()
    {
        _rb.velocity = _velocity * Time.fixedDeltaTime * _speed;
    }

    private void LookAtMouse()
    {
        RaycastHit hit;
        Ray ray = _mainCam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 100))
        {
            Vector3 p = new Vector3(hit.point.x, transform.position.y, hit.point.z);
            transform.LookAt(p);
        }
    }

    private void Fire()
    {
        // Do not shoot when level/ring is changing
        if (_speedMax <= 0)
        {
            return;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            _ballCount++;
            _ui.ShowBallCount(_ballCount);
            Transform p = Instantiate(_projectile, _projectileSpawnPoint.position, transform.rotation);
            p.parent = _projectileContainer;
        }
    }

    public void Hit()
    {
        ReloadLevel();
    }

    private void ReloadLevel()
    {
        SceneManager.LoadScene(0);
    }
}