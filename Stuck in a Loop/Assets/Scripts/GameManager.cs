﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Ring _ring;
    [SerializeField] private int _ringCount = 3;
    [SerializeField, Range(0f, 5f)] private float _ringChangingDuration = 0.5f;
    private List<Ring> _rings = new List<Ring>();
    private int _currentRingIndex = 0;
    private Camera _mainCam;
    private UIManager _ui;

    public Action onStartChangingRing;
    public Action onEndChangingRing;

    private void Start()
    {
        _mainCam = Camera.main;

        _ui = FindObjectOfType<UIManager>();
        if (_ui == null) Debug.Log("<color=lime>No UIManager found!</color>");

        for (int i = 0; i < _ringCount; i++)
        {
            Transform t = Instantiate(_ring.transform, transform.position, Quaternion.identity);
            t.name = string.Format("Ring_{0}", i);
            t.transform.position += new Vector3(0, i * 10, 0);
            t.transform.rotation = Quaternion.Euler(0, i * 180, 0);
            if (i == 0) t.localScale = new Vector3(1, 1, 1);
            else t.localScale = new Vector3(2, 2, 2);

            Ring r = t.GetComponent<Ring>();
            r.lerpTime = _ringChangingDuration;
            _rings.Add(r);
        }
    }

    public void NextRing()
    {
        StartCoroutine(IERingChanging(_ringChangingDuration));
        _currentRingIndex++;
        _ui.ShowLevel(_currentRingIndex);
        _ui.ShowLevel(_currentRingIndex);
        for (int i = 0; i < _rings.Count; i++)
        {
            Ring ring = _rings[i];
            ring.MoveDown(i <= _currentRingIndex);
        }
    }

    private IEnumerator IERingChanging(float wait)
    {
        onStartChangingRing?.Invoke();
        yield return new WaitForSeconds(wait + 0.1f);
        onEndChangingRing?.Invoke();
    }
}