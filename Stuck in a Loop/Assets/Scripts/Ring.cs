﻿using System.Collections;
using UnityEngine;

public class Ring : MonoBehaviour
{
    private Collider[] _colliders;
    float _lerpTime = 0.5f; // duration of lerping

    public float lerpTime { get => _lerpTime; set => _lerpTime = value; }

    private void Start()
    {
        _colliders = transform.GetComponentsInChildren<Collider>();
    }

    public void MoveDown(bool scale = false)
    {
        StartCoroutine(IEScale(scale));
    }

    private IEnumerator IEScale(bool scale = false)
    {
        float currentLerpTime = 0f; ;
        Vector3 startScale = transform.localScale;
        Vector3 endScale = startScale * 0.5f;
        Vector3 startPos = transform.position;
        Vector3 endPos = startPos + new Vector3(0, -10, 0);
        bool run = true;

        while (run)
        {
            // increment timer once per frame
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > _lerpTime)
            {
                currentLerpTime = _lerpTime;
            }

            // lerp!
            float t = currentLerpTime / lerpTime;
            t = t = t * t * (3f - 2f * t); // Easing: Smoothstep
            transform.position = Vector3.Lerp(startPos, endPos, t);
            if (scale) transform.localScale = Vector3.Lerp(startScale, endScale, t);

            if (t >= 1)
            {
                run = false;
            }

            yield return null;
        }
    }
}