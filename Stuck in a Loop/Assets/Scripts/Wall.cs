﻿using System.Collections;
using UnityEngine;

public class Wall : MonoBehaviour
{
    [SerializeField] private int _hitPoints = 50;
    [SerializeField] private bool _breakable;
    [SerializeField] private Material _breakableMaterial;
    [SerializeField] private Material _unbreakableMaterial;
    private int _hitPointsMax;
    private Material _mat;
    private Color _colorStart = Color.green;
    private Color _colorEnd = Color.red;
    private GameManager _gameManager;

    public bool breakable  => _breakable;

    private void Start()
    {
        _hitPointsMax = _hitPoints;

        _gameManager = FindObjectOfType<GameManager>();
        if (_gameManager == null) Debug.Log("<color=lime>No GameManager found!</color>");

        MeshRenderer rend = GetComponent<MeshRenderer>();
        if (_breakable) rend.material = _breakableMaterial;
        else rend.material = _unbreakableMaterial;
        _mat = rend.material;

        if (_breakable) StartCoroutine(IEHeal());
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!_breakable)
        {
            return;
        }

        if (collision.gameObject.CompareTag("Projectile"))
        {
            UpdateHitpoints(_hitPoints - 1);
        }
    }

    private void UpdateHitpoints(int hitPoints)
    {
        _hitPoints = hitPoints;

        float t = 1 - ((float)_hitPoints / _hitPointsMax);
        _mat.SetColor("_BaseColor", Color.Lerp(_colorStart, _colorEnd, t));

        if (_hitPoints <= 0)
        {
            _gameManager.NextRing();
            Destroy(gameObject, 0.01f);
        }
    }

    private IEnumerator IEHeal()
    {
        float healSpeed = 5;
        while (true)
        {
            UpdateHitpoints(Mathf.Clamp(_hitPoints + 1, 0, _hitPointsMax));
            yield return new WaitForSeconds(healSpeed);
        }
    }

#if UNITY_EDITOR

    private void OnDrawGizmos()
    {
        if (_breakable)
        {
            UnityEditor.Handles.Label(transform.position, "Breakable");
        }
    }

#endif
}